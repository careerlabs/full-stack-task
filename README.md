![](https://i.imgur.com/KSbsTd5.png)

## FullStack Developer Challenge

This weburl containing a csv list of course listings [https://nut-case.s3.amazonaws.com/courses.csv](https://nut-case.s3.amazonaws.com/courses.csv)


Develope a pseudo fullstack application for MOOC course listings
your are expected to use the dump provided and write e2e web app 


Build an application to search and list Course listings


## Requirements and Output

- Use of CSV file and Dump it in the DB
- Write a functional backend and frontend in the tech stack mentioned below
- List all the courses on the front end

### Features

- User is able to sort based on Next Session Date and Length
- User is able to filter based on Provider

## Good to have 
- User select the Next Session Date or Child Subject or Provider to search Courses


## Nice to have

- SSO Implemantation


####  Tech Stack [LAMP or MERN]: 
- PHP >= 7.x / NodeJS >= 9.x
- ReactJS/Angular/Vuejs
- DB of your choice


## Conditions

- Data exchange/sharing between the backend and front end must be in json via **REST API**
- If you make any assumptions while solving the exercise please mention them clearly in the readme file

## What we are looking for

- **Simple readable code** How well structured it is? Clear separation of concerns? Can anyone just look at it and get the idea to
what is being done? Does it follow any standards?
- **Correctness** Does the application do what it promises? Can we find bugs or trivial flaws?
- **Memory efficiency** How will it behave in case of large datasets?
- **Documentation** Is the code self documented and it's easy to understand it by just reading?
- **Testing** How well tested your application is? Can you give some metrics?


## Commit History
We just ask that you keep your commit history as coherent as possible

## Questions & Delivery

If you have any questions to this challenge, please do reach out to us.

The challenge should be delivered as a link to a public git repository (gitlab.com or bitbucket.com or github are preferred).


## Checklist

Before submitting, make sure that your program

- [ ] Code accompanies the Unit Tests
- [ ] Deployment URL 
- [ ] Usage is clearly mentioned in the README file, This including setup the project, how to run it, how to run unit test, examples,etc



## Note

Implementations focusing on **quality over feature completeness** will be highly appreciated,  don’t feel compelled to implement everything and even if you are not able to complete the challenge, please do submit it anyways.
